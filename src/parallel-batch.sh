#!/bin/sh

# openmpi style batch script (200422)

# number of nodes and tasks per node
#SBATCH -N 1
# for parallel package, specify cores
##SBATCH --tasks-per-node=20
# for rslurm package, use 1 core
#SBATCH --tasks-per-node=1
#SBATCH --mem-per-cpu=3100
#SBATCH --exclusive

# run time, 1 min
#SBATCH -t 00:01:00

# job name in queue
#SBATCH -J primes

# output and error file
#SBATCH -o tmp/result_%j.out
#SBATCH -e tmp/result_%j.err

# messaging
#SBATCH --mail-user=nils.holmberg@yahoo.com
#SBATCH --mail-type=END

cat $0

# module load statement(s) - adopt for your package
#module load foss/2016a
#module load rstudio/1.2.5019
module load GCC/8.2.0-2.31.1 OpenMPI/3.1.3 R/3.6.0

# run the jobs - here we assume an executable name: my_program
#mpirun -bind-to core src/parallel.sh
Rscript --vanilla src/parallel.R
