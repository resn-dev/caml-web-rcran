#!/bin/sh

# openmpi style batch script (200422)

# number of nodes and tasks per node
#SBATCH -N 1
#SBATCH --tasks-per-node=20
#SBATCH --exclusive

# run time, 1 min
#SBATCH -t 0:01:0

# job name in queue
#SBATCH -J parallel-prime-test

# output and error file
#SBATCH -o result_%j.out
#SBATCH -e result_%j.err

cat $0

# module load statement(s) - adopt for your package
module load foss/2016a
#
module load GCC/4.9.3-2.25  OpenMPI/1.10.2
module load GCC/4.9.3-binutils-2.25  OpenMPI/1.8.8
module load ScaLAPACK/2.0.2-OpenBLAS-0.2.15-LAPACK-3.6.0
module load ScaLAPACK/2.0.2-OpenBLAS-0.2.14-LAPACK-3.5.0
#
module load ncurses/6.0
#
module load GCC/4.8.4  OpenMPI/1.8.4
module load GCC/4.9.2  OpenMPI/1.8.4
module load GCC/4.9.3-2.25  OpenMPI/1.10.2
module load GCC/4.9.3-binutils-2.25  OpenMPI/1.8.8
module load GCC/5.4.0-2.26  OpenMPI/1.10.3
module load icc/2016.1.150-GCC-4.9.3-2.25  impi/5.1.2.150
module load icc/2016.3.210-GCC-4.9.3-2.25  OpenMPI/1.10.3
module load ifort/2016.1.150-GCC-4.9.3-2.25  impi/5.1.2.150
module load ifort/2016.3.210-GCC-4.9.3-2.25  OpenMPI/1.10.3
module load FFTW/3.3.4
#module load rstudio/1.2.5019
module load GCC/8.2.0-2.31.1 OpenMPI/3.1.3
module load R/3.6.0

# run the jobs - here we assume an executable name: my_program
mpirun -bind-to core src/parallel.sh
