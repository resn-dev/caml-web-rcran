#!/bin/sh

#R CMD BATCH src/parallel.R
# disable save workspace image on exit
#R CMD BATCH --no-save --no-restore src/parallel.R tmp/parallel.out
#R CMD BATCH --no-save --no-restore '--args a=1 b=c(2,5,6)' test.R test.out &
#/sw/pkg/rstudio/1.2.5019/bin/R --vanilla --slave -f "src/parallel.R"
#R --vanilla --slave -f "src/parallel.R"
Rscript --vanilla src/parallel.R
