#!/bin/sh

# intelmpi style batch script (200422)

# number of nodes and tasks per node
#SBATCH -N 1
#SBATCH --tasks-per-node=20
#SBATCH --exclusive

# run time, 1 min
#SBATCH -t 0:01:0

# job name in queue
#SBATCH -J parallel_prime_test

# output and error file
#SBATCH -o result_%j.out
#SBATCH -e result_%j.err

cat $0

# module load statement(s) - adopt for your package
module load intel/2016a
module load rstudio/1.2.5019

# run the jobs - here we assume an executable name: my_program
srun src/parallel.sh
